import { useState } from "react";
import Modal from "react-modal";

const MODAL_LOGIN = 1;
const MODAL_SINGUP = 2;

function App() {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [whichModal, setWhichModal] = useState(null);

  const handleLogin = (event) => {
    event.preventDefault();
    setIsModalOpen(false);
  };

  return (
    <div>
      <h1>Learn React Modal</h1>
      <hr />
      <button
        onClick={() => {
          setIsModalOpen(true);
          setWhichModal(MODAL_SINGUP);
        }}
      >
        Sign UP
      </button>

      <button
        onClick={() => {
          setIsModalOpen(true);
          setWhichModal(MODAL_LOGIN);
        }}
      >
        Log In
      </button>

      <Modal
        isOpen={isModalOpen}
        onRequestClose={() => setIsModalOpen(false)}
        className="modal__container"
        overlayClassName="modal__overlay--center"
        contentLabel="Learn Modal"
      >
        {rederWhichModal()}
      </Modal>
    </div>
  );

  function rederWhichModal() {
    switch (whichModal) {
      case MODAL_SINGUP:
        return (
          <div className="home__signup">
            <h1> - MilanTV - </h1>
            <form className="home__signup__form" onSubmit={handleLogin}>
              <div>Full Name</div>
              <input type="text" placeholder="username or email" />

              <div>Email</div>
              <input type="email" placeholder="password" />

              <div>Password</div>
              <input type="password" placeholder="password" />

              <button className="home__signup__form__submit" type="submit">
                Login
              </button>
            </form>

            <h2 className="home__signup__redirect">
              Already Have Account,{" "}
              <span onClick={() => setWhichModal(MODAL_LOGIN)}>Login Here</span>
            </h2>
          </div>
        );
      case MODAL_LOGIN:
        return <div>ini log in</div>;
      default:
        break;
    }
  }
}

export default App;
